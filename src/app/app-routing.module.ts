import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LocalBillingComponent } from './domestic/local-billing/local-billing.component';
import { BillListComponent } from './domestic/bill-list/bill-list.component'
import { BillEditComponent } from './domestic/bill-edit/bill-edit.component';
import { ViewInvoiceComponent } from './domestic/view-invoice/view-invoice.component'
import { HomeComponent } from './home/home.component';
import { ListComponent } from './international/list/list.component';
import { CreateComponent } from './international/create/create.component';
import { EditComponent } from './international/edit/edit.component';
import { ViewComponent } from './international/view/view.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  { path: '', component: LoginComponent, pathMatch: 'full' },
  { path: 'dashboard', component: HomeComponent, pathMatch: 'full' },
  { path: 'domestic', component: BillListComponent, pathMatch: 'full' },
  { path: 'domestic/create', component: LocalBillingComponent, pathMatch: 'full'},
  { path: 'domestic/edit/:id', component: BillEditComponent, pathMatch: 'full' },
  { path: 'domestic/view/:id', component: ViewInvoiceComponent, pathMatch: 'full' },
  { path: 'international', component: ListComponent, pathMatch: 'full' },
  { path: 'international/create', component: CreateComponent, pathMatch: 'full' },
  { path: 'international/edit/:id', component: EditComponent, pathMatch: 'full' },
  { path: 'international/view/:id', component: ViewComponent, pathMatch: 'full' },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
