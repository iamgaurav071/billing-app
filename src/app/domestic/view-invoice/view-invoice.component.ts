import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MessageService } from 'primeng/api';

declare var html2pdf: any

@Component({
  selector: 'app-view-invoice',
  templateUrl: './view-invoice.component.html',
  styleUrls: ['./view-invoice.component.css']
})
export class ViewInvoiceComponent implements OnInit {

  invoiceDetails: any;
  loader: boolean = false

  constructor(private messageService: MessageService, private route: ActivatedRoute, private router: Router, private cs: CommonService) { 
  }

  public loadScript(url: string) {
    const body = <HTMLDivElement>document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

  ngOnInit(): void {
    this.loadScript('../../assets/js/html2pdf.bundle.min.js')
    this.getInvoiceById(this.route.snapshot.params['id'])
  }

  getInvoiceById(id) {
    this.loader = true
    this.cs.getInvoiceById({id: id}).subscribe((res: any)=> {
      this.invoiceDetails = res.data
      this.loader = false
    }, (err: HttpErrorResponse)=> {
      this.messageService.add({severity:'error', summary: 'Error', detail: "Something went wrong"});
    })
  }

  getTaxValue(value) {
    if (this.invoiceDetails.state == 'Punjab') return this.twoDigitDecimals(parseFloat(value)/2)
    else return 0
  }

  twoDigitDecimals(data: number){
    return (Math.round(data * 100) / 100).toFixed(2);
  }

  calculateTaxableValue(){
    let totalCount = 0

    if (this.invoiceDetails && this.invoiceDetails.productDetails) {
      let data = this.invoiceDetails.productDetails
      data.forEach(element => {
        totalCount = totalCount + parseFloat(element.taxableValue)
      });
    }

    return this.twoDigitDecimals(totalCount)
  }

  getTax(val) {
    if (val == '1.05') return '5%'
    if (val == '1.12') return '12%'
    if (val == '1.18') return '18%'
    else return 0
  }

  taxFiveCalulate(){
    if (this.invoiceDetails && this.invoiceDetails.productDetails) {
      let count = 0
      let data = this.invoiceDetails.productDetails
      data.forEach(element => {
        if (element.tax == '1.05') {
          count += parseFloat(element.taxAmount)
        }
      });

      return this.twoDigitDecimals(count)
    }
  }

  taxTwelveCalulate() {
    if (this.invoiceDetails && this.invoiceDetails.productDetails) {
      let count = 0
      let data = this.invoiceDetails.productDetails
      data.forEach(element => {
        if (element.tax == '1.12') {
          count += parseFloat(element.taxAmount)
        }
      });
      return this.twoDigitDecimals(count)
    }
    return 0
  }

  taxEighteenCalulate() {
    if (this.invoiceDetails && this.invoiceDetails.productDetails) {
      let count = 0
      let data = this.invoiceDetails.productDetails
      data.forEach(element => {
        if (element.tax == '1.18') {
          count += parseFloat(element.taxAmount)
        }
      });
      return this.twoDigitDecimals(count)
    }
    return 0
  }

  totalAmount() {
    if (this.invoiceDetails && this.invoiceDetails.productDetails) {
      let count = 0
      let data = this.invoiceDetails.productDetails
      data.forEach(element => {
        count += parseFloat(element.netAmount)
      });
      return this.twoDigitDecimals(count)
    }
    return 0
  }

  print() {
    var element = document.getElementById('pdfGenerate');
    var opt = {
      margin: 0.2,
      filename: `${this.invoiceDetails.invoiceSerialNumber}-invoice.pdf`,
      image: { type: 'png', quality: 1 },
      html2canvas: { scale: 1 },
      pagebreak: { avoid: 'tr' },
      jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' }
    };
    html2pdf().from(element).set(opt).save()
  }

}
