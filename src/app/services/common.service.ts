import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private httpClient: HttpClient) { }

  getAllStates(){
    return [
      {"name":"Andhra Pradesh"},
      {"name":"Arunachal Pradesh"},
      {"name":"Assam"},
      {"name": "Andaman and Nicobar Islands"},
      {"name":"Bihar"},
      {"name":"Chhattisgarh"},
      {"name":"Chandigarh"},
      {"name":"Dadra and Nagar Haveli"},
      {"name":"Daman and Diu"},
      {"name":"Delhi"},
      {"name":"Goa"},
      {"name":"Gujarat"},
      {"name":"Haryana"},
      {"name":"Himachal Pradesh"},
      {"name":"Jammu and Kashmir"},
      {"name":"Jharkhand"},
      {"name":"Karnataka"},
      {"name":"Kerala"},
      {"name":"Ladakh"},
      {"name":"Lakshadweep"},
      {"name":"Madhya Pradesh"},
      {"name":"Maharashtra"},
      {"name":"Manipur"},
      {"name":"Meghalaya"},
      {"name":"Mizoram"},
      {"name":"Nagaland"},
      {"name":"Odisha"},
      {"name":"Punjab"},
      {"name":"Puducherry"},
      {"name":"Rajasthan"},
      {"name":"Sikkim"},
      {"name":"Tamil Nadu"},
      {"name":"Telangana"},
      {"name":"Tripura"},
      {"name":"Uttar Pradesh"},
      {"name":"Uttarakhand"},
      {"name":"West Bengal"}
    ]
  }

  addInvoices(obj) {
    return this.httpClient.post(environment.BASEURL+'add-invoice', obj)
  }

  getAllInvoices(){
    return this.httpClient.post(environment.BASEURL+'getinvoices', {})
  }

  deleteInvoice(obj) {
    return this.httpClient.post(environment.BASEURL+'delete-invoices', obj)
  }

  getInvoiceById(obj){
    return this.httpClient.post(environment.BASEURL+'getinvoicesbyid', obj)
  }

  updateInvoice(obj) {
    return this.httpClient.post(environment.BASEURL+'update-invoice', obj)
  }

  getAllInternationalInvoices() {
    return this.httpClient.post(environment.BASEURL+'getall', {})
  }

  addInternationalInvoices(obj) {
    return this.httpClient.post(environment.BASEURL+'add', obj)
  }

  deleteInternationalInvoice(obj) {
    return this.httpClient.post(environment.BASEURL+'delete', obj)
  }

  getInternationalInvoiceById(obj){
    return this.httpClient.post(environment.BASEURL+'edit', obj)
  }

  updateInternationalInvoice(obj) {
    return this.httpClient.post(environment.BASEURL+'update', obj)
  }
}
