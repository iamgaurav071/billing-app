import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalBillingComponent } from './local-billing.component';

describe('LocalBillingComponent', () => {
  let component: LocalBillingComponent;
  let fixture: ComponentFixture<LocalBillingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocalBillingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalBillingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
