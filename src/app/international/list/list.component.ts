import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MessageService } from 'primeng/api';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  invoiceList: any;
  loader: boolean;

  constructor(private cs: CommonService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.getAllInvoices()
  }

  getAllInvoices(){
    this.loader = true
    this.cs.getAllInternationalInvoices().subscribe((res: any)=> {
      this.invoiceList = res.data
      this.loader = false
    }, (err: HttpErrorResponse)=> {
      this.messageService.add({severity:'error', summary: 'Error', detail: "Something went wrong"});
    })
  }

  deleteInvoice(id) {
    this.cs.deleteInternationalInvoice({id: id}).subscribe((res: any)=> {
      this.messageService.add({severity:'success', summary: 'Invoices', detail: 'Deleted Successfully'});
      this.getAllInvoices()
    }, (err: HttpErrorResponse)=> {
      this.messageService.add({severity:'error', summary: 'Error', detail: "Something went wrong"});
    })
  }

}
