import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray ,Validators } from '@angular/forms'
import { CommonService } from '../../services/common.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router'
import { MessageService } from 'primeng/api';

interface State {
  name: string,
  code: string
}

@Component({
  selector: 'app-bill-edit',
  templateUrl: './bill-edit.component.html',
  styleUrls: ['./bill-edit.component.css']
})
export class BillEditComponent implements OnInit {
  billingForm: FormGroup
  date1: Date;
  states: any;
  invoiceDetails: any

  loader: boolean = false

  constructor(private messageService: MessageService, private fb: FormBuilder, private cs: CommonService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.initForm()
    this.getAllStates()
    this.getInvoiceById(this.route.snapshot.params['id'])
  }

  getInvoiceById(id) {
    this.loader = true
    this.cs.getInvoiceById({id: id}).subscribe((res: any)=> {
      this.invoiceDetails = res.data

      this.billingForm.patchValue({
        state: this.invoiceDetails.state,
        gstinNumber: this.invoiceDetails.gstinNumber,
        invoiceSerialNumber: this.invoiceDetails.invoiceSerialNumber,
        invoiceDate: new Date(this.invoiceDetails.invoiceDate),
        pincode: this.invoiceDetails.pincode,
        paymentMethod: this.invoiceDetails.paymentMethod,
        adCode: this.invoiceDetails.adCode,
        shippingCompany: this.invoiceDetails.shippingCompany,
        trackingNumber: this.invoiceDetails.trackingNumber,
        name: this.invoiceDetails.name,
        address: this.invoiceDetails.address,
        phone: this.invoiceDetails.phone,
        country: this.invoiceDetails.country,
        customerGstinNumber: this.invoiceDetails.customerGstinNumber,
        isAddressSame: this.invoiceDetails.isAddressSame,
        billingName: this.invoiceDetails.billingName,
        billingAddress: this.invoiceDetails.billingAddress,
        billingPhone: this.invoiceDetails.billingPhone,
        billingState: this.invoiceDetails.billingState,
        billingPincode: this.invoiceDetails.billingPincode,
        billingCountry: this.invoiceDetails.billingCountry
      })

      this.billingForm.setControl("productDetails", this.getProductDetails(this.invoiceDetails.productDetails))
      this.loader = false
    }, (err: HttpErrorResponse)=> {
      this.messageService.add({severity:'error', summary: 'Error', detail: "Something went wrong"});
    })
  }

  getProductDetails(result: any[]): FormArray {
    const formArray = new FormArray([]);
      result.forEach((s, i) => {
        formArray.push(
          this.fb.group({
            productName: s.productName,
            hsn: s.hsn,
            quantity: s.quantity,
            uom:s.uom,
            rate: s.rate,
            discount: s.discount,
            tax: s.tax,
            taxableValue: s.taxableValue,
            taxAmount: s.taxAmount,
            netAmount: s.netAmount
          })
        );
      });

    return formArray
  }

  getAllStates(){
    this.states = this.cs.getAllStates()
  }

  initForm(){
    this.billingForm = this.fb.group({
      state: ['', [Validators.required]],
      gstinNumber: ['03IWWPS6580R1Z0', [Validators.required]],
      invoiceSerialNumber: [null, [Validators.required]],
      invoiceDate: [new Date(), [Validators.required]],
      paymentMethod: ["", [Validators.required]],
      adCode: ['6380014', [Validators.required]],
      shippingCompany: ['', [Validators.required]],
      trackingNumber: [null],
      name: [null, [Validators.required]],
      address: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      pincode: [null, [Validators.required]],
      country: [null, [Validators.required]],
      customerGstinNumber: [null],
      isAddressSame: [false],
      billingName: [null, [Validators.required]],
      billingAddress: [null, [Validators.required]],
      billingPhone: [null, [Validators.required]],
      billingPincode: [null, [Validators.required]],
      billingState: [null, [Validators.required]],
      billingCountry: [null, [Validators.required]],
      productDetails: this.fb.array([this.initProductDetailsArray])
    })
  }

  get initProductDetailsArray(): FormGroup{
    return this.fb.group({
      productName: [null],
      hsn: [''],
      quantity: [0],
      uom: [''],
      rate: [0],
      discount: [0],
      tax: [0],
      taxableValue: [0],
      taxAmount: [0],
      netAmount: [0]
    });
  }

  calculateDiscount(){
    this.billingForm.setControl("productDetails", this.getProductDetailsForEdit(this.billingForm.value.productDetails))
  }

  twoDigitDecimals(data: number){
    return (Math.round(data * 100) / 100).toFixed(2);
  }

  getProductDetailsForEdit(result: any[]): FormArray {
    const formArray = new FormArray([]);
      result.forEach((s, i) => {
        formArray.push(
          this.fb.group({
            productName: s.productName,
            hsn: s.hsn,
            quantity: s.quantity,
            uom:s.uom,
            rate: s.rate,
            discount: s.discount,
            tax: s.tax,
            taxAmount: this.twoDigitDecimals(((s.rate - ((s.rate*s.discount)/100)) - ((s.rate - ((s.rate*s.discount)/100))/s.tax)) * s.quantity),
            taxableValue: this.twoDigitDecimals(((s.rate - ((s.rate*s.discount)/100)) - ((s.rate - ((s.rate*s.discount)/100)) - ((s.rate - ((s.rate*s.discount)/100))/s.tax))) * s.quantity),
            netAmount: (s.rate - ((s.rate*s.discount)/100)) * s.quantity
          })
        );
      });

    return formArray
  }

  addProductDetailsForm() {
    (this.billingForm.get("productDetails") as FormArray).push(
      this.initProductDetailsArray
    );
  }

  removeProductDetailsForm(i){
    (this.billingForm.get("productDetails") as FormArray).removeAt(i);
  }

  updateInvoice(){

    if (
      this.billingForm.controls.state.invalid || this.billingForm.controls.gstinNumber.invalid || 
      this.billingForm.controls.invoiceSerialNumber.invalid || this.billingForm.controls.invoiceDate.invalid ||
      this.billingForm.controls.paymentMethod.invalid || this.billingForm.controls.adCode.invalid ||
      this.billingForm.controls.name.invalid || this.billingForm.controls.address.invalid ||
      this.billingForm.controls.phone.invalid || this.billingForm.controls.pincode.invalid ||
      this.billingForm.controls.country.invalid || this.billingForm.controls.billingName.invalid ||
      this.billingForm.controls.billingPhone.invalid || this.billingForm.controls.billingPincode.invalid ||
      this.billingForm.controls.billingState.invalid || this.billingForm.controls.billingCountry.invalid
    ) {
      Object.keys(this.billingForm.controls).forEach(key => {
        this.billingForm.get(key).markAsDirty();
      });

      return 
  }

    this.loader = true

    let object = {
      id: this.invoiceDetails._id,
      state: this.billingForm.value.state,
      gstinNumber: this.billingForm.value.gstinNumber,
      invoiceSerialNumber: this.billingForm.value.invoiceSerialNumber,
      invoiceDate: this.billingForm.value.invoiceDate,
      paymentMethod: this.billingForm.value.paymentMethod,
      adCode: this.billingForm.value.adCode,
      shippingCompany: this.billingForm.value.shippingCompany,
      trackingNumber: this.billingForm.value.trackingNumber,
      name: this.billingForm.value.name,
      address: this.billingForm.value.address,
      phone: this.billingForm.value.phone,
      pincode: this.billingForm.value.pincode,
      country: this.billingForm.value.country,
      customerGstinNumber: this.billingForm.value.customerGstinNumber,
      isAddressSame: this.billingForm.value.isAddressSame,
      billingName: (this.billingForm.value.isAddressSame) ? this.billingForm.value.name : this.billingForm.value.billingName,
      billingAddress: (this.billingForm.value.isAddressSame) ? this.billingForm.value.address :this.billingForm.value.billingAddress,
      billingPhone: (this.billingForm.value.isAddressSame) ? this.billingForm.value.phone :this.billingForm.value.billingPhone,
      billingPincode: (this.billingForm.value.isAddressSame) ? this.billingForm.value.pincode :this.billingForm.value.billingPincode,
      billingState: (this.billingForm.value.isAddressSame) ? this.billingForm.value.state :this.billingForm.value.billingState,
      billingCountry: (this.billingForm.value.isAddressSame) ? this.billingForm.value.country :this.billingForm.value.billingCountry,
      productDetails: this.billingForm.get('productDetails').value
    }

    this.cs.updateInvoice(object).subscribe((res: any)=> {
      this.messageService.add({severity:'success', summary: 'Invoice', detail: "Updated Successfully"});
      this.loader = false
      this.router.navigate(['/domestic'])
    }, (err:HttpErrorResponse)=> {
      this.messageService.add({severity:'error', summary: 'Error', detail: "Something went wrong"});
    })
  }

  isAddressSameOnChange(){
    if (this.billingForm.value.isAddressSame) {
      this.billingForm.patchValue({
        billingName: this.billingForm.value.name,
        billingAddress: this.billingForm.value.address,
        billingPhone: this.billingForm.value.phone,
        billingPincode: this.billingForm.value.pincode,
        billingState: this.billingForm.value.state,
        billingCountry: this.billingForm.value.country
      })
    } else {
      this.billingForm.patchValue({
        billingName: null,
        billingAddress: null,
        billingPhone: null,
        billingPincode: null,
        billingState: null,
        billingCountry: null
      })
    }
  }
}
