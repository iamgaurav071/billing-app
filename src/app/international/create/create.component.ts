import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormGroup, FormBuilder, FormArray ,Validators } from '@angular/forms'
import { CommonService } from '../../services/common.service'
import { Router } from '@angular/router'
import { MessageService } from 'primeng/api';

interface State {
  name: string,
  code: string
}

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  billingForm: FormGroup
  date1: Date;
  states: any;
  loader: boolean;

  constructor(private messageService: MessageService, private fb: FormBuilder, private cs: CommonService, private router: Router) { }

  ngOnInit(): void {
    this.initForm()
    this.getAllStates()
  }

  getAllStates(){
    this.states = this.cs.getAllStates()
  }

  initForm(){
    this.billingForm = this.fb.group({
      gstinNumber: ['03IWWPS6580R1Z0', [Validators.required]],
      invoiceSerialNumber: [null, [Validators.required]],
      invoiceDate: [new Date(), [Validators.required]],
      paymentMethod: ["", [Validators.required]],
      currency: ["INR", [Validators.required]],
      adCode: ['6380014', [Validators.required]],
      shippingCompany: ['', [Validators.required]],
      trackingNumber: [null],
      name: [null, [Validators.required]],
      address: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      pincode: [null, [Validators.required]],
      state: ['', [Validators.required]],
      country: [null, [Validators.required]],
      customerGstinNumber: [null],
      isAddressSame: [false],
      billingName: [null, [Validators.required]],
      billingAddress: [null, [Validators.required]],
      billingPhone: [null, [Validators.required]],
      billingPincode: [null, [Validators.required]],
      billingState: [null, [Validators.required]],
      billingCountry: [null, [Validators.required]],
      productDetails: this.fb.array([this.initProductDetailsArray])
    })
  }

  get initProductDetailsArray(): FormGroup{
    return this.fb.group({
      productName: [null],
      hsn: [''],
      quantity: [0],
      uom: [''],
      rate: [0],
      discount: [0],
      netAmount: [0]
    });
  }

  addProductDetailsForm() {
    (this.billingForm.get("productDetails") as FormArray).push(
      this.initProductDetailsArray
    );
  }

  isAddressSameOnChange(){
    if (this.billingForm.value.isAddressSame) {
      this.billingForm.patchValue({
        billingName: this.billingForm.value.name,
        billingAddress: this.billingForm.value.address,
        billingPhone: this.billingForm.value.phone,
        billingPincode: this.billingForm.value.pincode,
        billingState: this.billingForm.value.state,
        billingCountry: this.billingForm.value.country
      })
    } else {
      this.billingForm.patchValue({
        billingName: null,
        billingAddress: null,
        billingPhone: null,
        billingPincode: null,
        billingState: null,
        billingCountry: null
      })
    }
  }

  removeProductDetailsForm(i){
    (this.billingForm.get("productDetails") as FormArray).removeAt(i);
  }

  addBill(){

    if (
      this.billingForm.controls.state.invalid || this.billingForm.controls.gstinNumber.invalid || 
      this.billingForm.controls.invoiceSerialNumber.invalid || this.billingForm.controls.invoiceDate.invalid ||
      this.billingForm.controls.paymentMethod.invalid || this.billingForm.controls.adCode.invalid ||
      this.billingForm.controls.name.invalid || this.billingForm.controls.address.invalid ||
      this.billingForm.controls.phone.invalid || this.billingForm.controls.pincode.invalid ||
      this.billingForm.controls.country.invalid || this.billingForm.controls.billingName.invalid ||
      this.billingForm.controls.billingPhone.invalid || this.billingForm.controls.billingPincode.invalid ||
      this.billingForm.controls.billingState.invalid || this.billingForm.controls.billingCountry.invalid
    ) {
      Object.keys(this.billingForm.controls).forEach(key => {
        this.billingForm.get(key).markAsDirty();
      });

      return 
    }

    let object = {
      gstinNumber: this.billingForm.value.gstinNumber,
      invoiceSerialNumber: this.billingForm.value.invoiceSerialNumber,
      invoiceDate: this.billingForm.value.invoiceDate,
      currency: this.billingForm.value.currency,
      paymentMethod: this.billingForm.value.paymentMethod,
      adCode: this.billingForm.value.adCode,
      shippingCompany: this.billingForm.value.shippingCompany,
      trackingNumber: this.billingForm.value.trackingNumber,
      name: this.billingForm.value.name,
      address: this.billingForm.value.address,
      phone: this.billingForm.value.phone,
      pincode: this.billingForm.value.pincode,
      state: this.billingForm.value.state,
      country: this.billingForm.value.country,
      isAddressSame: this.billingForm.value.isAddressSame,
      billingName: (this.billingForm.value.isAddressSame) ? this.billingForm.value.name : this.billingForm.value.billingName,
      billingAddress: (this.billingForm.value.isAddressSame) ? this.billingForm.value.address :this.billingForm.value.billingAddress,
      billingPhone: (this.billingForm.value.isAddressSame) ? this.billingForm.value.phone :this.billingForm.value.billingPhone,
      billingPincode: (this.billingForm.value.isAddressSame) ? this.billingForm.value.pincode :this.billingForm.value.billingPincode,
      billingState: (this.billingForm.value.isAddressSame) ? this.billingForm.value.state :this.billingForm.value.billingState,
      billingCountry: (this.billingForm.value.isAddressSame) ? this.billingForm.value.country :this.billingForm.value.billingCountry,
      productDetails: this.billingForm.get('productDetails').value
    }

    this.cs.addInternationalInvoices(object).subscribe((res: any)=> {
      if (res.message === "Duplicate invoice number") {
        this.messageService.add({severity:'warn', summary: 'Invoice', detail: "Duplicate invoice serial number"});
      } else {
        this.router.navigate(['/international'])
        this.messageService.add({severity:'success', summary: 'Invoice', detail: "Created Successfully"});
      }
      this.loader = false
    }, (err: HttpErrorResponse)=> {
      this.messageService.add({severity:'error', summary: 'Error', detail: "Something went wrong"});
    })
  }

  calculateDiscount() {
    this.billingForm.setControl("productDetails", this.getProductDetails(this.billingForm.value.productDetails))
  }

  twoDigitDecimals(data: number){
    return (Math.round(data * 100) / 100).toFixed(2);
  }

  getProductDetails(result: any[]): FormArray {
    const formArray = new FormArray([]);
      result.forEach((s, i) => {
        formArray.push(
          this.fb.group({
            productName: s.productName,
            hsn: s.hsn,
            quantity: s.quantity,
            uom:s.uom,
            rate: s.rate,
            discount: s.discount,
            netAmount: (s.rate - ((s.rate*s.discount)/100)) * s.quantity
          })
        );
      });

    return formArray
  }

}
