import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LocalBillingComponent } from './domestic/local-billing/local-billing.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

import { CalendarModule} from 'primeng/calendar';
import { BillListComponent } from './domestic/bill-list/bill-list.component';
import { BillEditComponent } from './domestic/bill-edit/bill-edit.component';
import { InterceptorService } from './services/interceptor.service';
import { ViewInvoiceComponent } from './domestic/view-invoice/view-invoice.component';
import { BlockUIModule } from 'primeng/blockui';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ToastModule } from 'primeng/toast';
import {RippleModule} from 'primeng/ripple';
import { MessageService } from 'primeng/api';
import { HomeComponent } from './home/home.component';
import { CreateComponent } from './international/create/create.component';
import { ListComponent } from './international/list/list.component';
import { EditComponent } from './international/edit/edit.component';
import { ViewComponent } from './international/view/view.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    LocalBillingComponent,
    HeaderComponent,
    FooterComponent,
    BillListComponent,
    BillEditComponent,
    ViewInvoiceComponent,
    HomeComponent,
    CreateComponent,
    ListComponent,
    EditComponent,
    ViewComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CalendarModule, FormsModule, ReactiveFormsModule,
    HttpClientModule, BlockUIModule, ProgressSpinnerModule, ToastModule, RippleModule
  ],
  providers: [MessageService, {provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
